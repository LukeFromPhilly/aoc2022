
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    file = open("../input.txt")
    num_list = list(file)
    elf = []
    calorie_list = []
    for num in num_list:
        if num != "\n":
            elf.append(int(num.replace("\n", "")))
        else:
            calorie_list.append(elf[:])
            elf.clear()
            continue

    most_calories = 0

    for items in calorie_list:
        if sum(items) > most_calories:
            most_calories = sum(items)

    print(most_calories)