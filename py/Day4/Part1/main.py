from copy import deepcopy


def least_area(n):
    return int(n[0])

if __name__ == "__main__":
    file = open("./input.txt")
    assignments = list(file)
    print(assignments)
    pair_list = list()
    pair_tuples = list()

    fullyContained = 0

    for pairs in assignments:
        pair_list.append(tuple(pairs.replace("\n", "").split(",")))

    for pair in pair_list:
        pair_tuples += tuple(pair[0].split("-")), tuple(pair[1].split("-"))

    assignment_pairs = list()

    for i in range(len(pair_tuples)):
        if i % 2 != 0:
            assignment_pairs.append([pair_tuples[i-1], pair_tuples[i]])
        else:
            continue

    sorted_pairs = []

    for i in range(len(assignment_pairs)):
        sorted_pairs.append(sorted(assignment_pairs[i], key=least_area))

    for p1, p2 in assignment_pairs:
        p1_min, p1_max, p2_min, p2_max = p1 + p2
        p1_min, p1_max, p2_min, p2_max = (int(x) for x in (p1_min, p1_max, p2_min, p2_max))
        if p1_max >= p2_max and p1_min >= p2_min:
            continue
        else:
            fullyContained += 1

    print(assignment_pairs)
    print(fullyContained)