package com.aoc.part1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static int priority(List<String> rucksack){
        int priority = 0;
        Character duplicatePouchElement = ' ';
        char first[] = rucksack.get(0).toCharArray();
        char second[] = rucksack.get(1).toCharArray();
        HashMap<Character, Integer> matches = new HashMap<>();
        Set<Character> firstSet = new TreeSet<Character>();
        Set<Character> secondSet = new TreeSet<Character>();
        for(int i = 0; i < first.length; i++){
            firstSet.add(first[i]);
            secondSet.add(second[i]);
        }

        for(Character c : firstSet){
                matches.put(c, 1);
        }

        for(Character c : secondSet){
            if(matches.containsKey(c)){
                matches.put(c, matches.get(c) + 1);
                break;
            }else{
                matches.put(c, 1);
            }
        }

        Map<Character, Integer> filteredSack = matches.entrySet()
                .stream().filter(x -> x.getValue() == 2)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        duplicatePouchElement = (Character) filteredSack.keySet().toArray()[0];

        if(Character.isUpperCase(duplicatePouchElement)){
            priority = (int) duplicatePouchElement - 64 + 26;
        }else{
            priority = (int) duplicatePouchElement - 96;
        }

        return priority;
    }

    public static void main(String[] args) {
        BufferedReader reader;
        List<List<String>> rucksacks = new ArrayList<>();
        ArrayList priorities = new ArrayList<>();
        int sum = 0;

        try{
            reader = new BufferedReader(new FileReader("./input.txt"));
            String line = reader.readLine();
            while (line != null){
                List compartments = List.of(line.substring(0, line.length()/2), line.substring(line.length()/2));
                rucksacks.add(compartments);
                line = reader.readLine();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

        for(List l: rucksacks){
            sum += priority(l);
        }

        System.out.println(sum);
    }
}
